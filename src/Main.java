import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1: Developer");
        System.out.println("2: Tester");
        System.out.print("Select your job title:");
        int title = scanner.nextInt();
        if (title == 1){
            Developer developer = new Developer();
            developer.work();
            System.out.println("Earn: "+developer.earn()+"/Year");
        }else if (title==2){
            Tester tester = new Tester();
            tester.work();
            System.out.println("Earn: "+tester.earn()+"/Year");
        }else {
            System.out.println("Job not existed");
        }
    }
}
class EmployeeWorking{
    double earn(){
       return 0.0;
    }
}
class Tester extends EmployeeWorking{
    double earn(){
        return 7000.0;
    }
    void work(){
        System.out.println("I'm a Tester.");
    }
}
class Developer extends EmployeeWorking{
    double earn(){
        return 10000.0;
    }
    void work(){
        System.out.println("I'm a Developer.");
    }
}